import React, { useContext } from 'react';
import { Text, View, FlatList } from 'react-native';
import { RootContext } from './jawaban2';

const Consumer = () => {
    const state = useContext(RootContext);
    return (
        <View>
            <FlatList data={state.name} renderItem={(dataItem) => {
                return (
                    <View style={{ marginVertical: 10, marginHorizontal: 10 }}>
                        <Text>Nama : {dataItem.item.name}</Text>
                        <Text>Posisi : {dataItem.item.position}</Text>
                    </View>
                )
            }}>

            </FlatList>
        </View>
    )
}
export default Consumer;